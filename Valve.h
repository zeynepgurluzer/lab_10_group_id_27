#pragma once
#include "Obs.h"
class Valve : public Observer
{
	/*! \class Valve Valve.h
	 *  \brief This is a valve class.
	 *
	 * Valve class has closing and opening controls.
	 * Valve class inherits Observer class.
	 *
	 */
private:
	/*! \brief This is a boolean variable.
	*
	*	"valve_status" controls tank's valve.
	*   If tank's valve is opened, then "valve" is true.
	*
	*/
	bool valve_status;

public:
	Valve();
	Valve(Subject *sub);

	/*! \brief This is a function sets valve_status.
	*
	*	@param status
	*   \return nothing
	*
	*/
	void set_status(bool status);

	/*! \brief This is a function opens the tank's valve.
	*
	*	@param tank_id
	*   \return nothing
	*
	*/
	void open_valve(int tank_id);

	/*! \brief This is a function closes tank's valve.
	*
	*	@param tank_id
	*   \return nothing
	*
	*/
	void close_valve(int tank_id); //int or Tank

	/*! \brief This is a function adds new valves.
	*
	*	@param index, *sub
	*   \return nothing
	*
	*/
	void add_valve(int, Subject*);

	/*! \brief This is a function gets  the valve's status.
	*
	*	@param nothing
	*   \return valve_status
	*
	*/
	bool get_status();

	/*! \brief This is a function about Observer.
	*
	*   @param nothing
	*   \return nothing
	* 
	*/
	void update();
};
/*! \brief This is a Valve object.
*
*	"valve" controls tank's valve.
*
*/
extern Valve* valve;
