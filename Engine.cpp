#include "Engine.h"
#include "Tank.h"
#include "Simulation.h"
#include <iostream>
#include "Valve.h"
#include "Output.h"
using namespace std;
string strr;
Engine::Engine():fuel_per_second(5.5) {}
Engine::Engine(Subject *sub) :Observer(sub), fuel_per_second(5.5) { //Const is defined because it says always 5.5
	status = 1;
	count_of_tank = 0; //counts tanks that have been connected
	internalTankcapacity = 55.0; //the capacity was set to 55 as requested.
	internalTankfuel = 0.0;
}
Engine::~Engine() {}
void Engine::start_engine() {
	statusControl(); ///To get the final status, status checks engine operation
	if (status == 0) {
		give_back_fuel(internalTankfuel);
		output.printFile("Any Tank is connected!");
	}

	else {
		output.printFile("The engine is started!");
	}

}
void Engine::stop_engine() {
	if (status == 0) {
		output.printFile("The Engine is already stopped!");
	}

	else {
		//gasoline in the internal tank is returned before the engine stops
		give_back_fuel(internalTankfuel);
		internalTankfuel = 0.0;
		status = 0;
		output.Endl();
		output.printFile("The Engine is stopped!");
	}
}
/**
*If there is any tank connected and the running cover is open, the engine runs and becomes status 1
*/
void Engine::statusControl() {
	int i, control = 0;
	bool flag = 0;
	if (tankcount == 0)
	{
		give_back_fuel(internalTankfuel);
		status = 0;
	}
	else {
		for (i = 0; i < tankcount; i++) {
			if (tankArray[i].getbroke(i) == 0 && tankArray[i].getIsconnect(i) == 1) {
				control++;
			}
			if (control == 1) {
				for (i = 0; i < tankcount; i++) {
					if (valve[i].get_status()==1) {
						status = 1;
						flag = 1;
						break;
					}
				}
			}
			else {
				if (flag != 1)
				{
					status = 0;
					control = 0;
				}
			}
		}
	}
}
int Engine::count_of_tank;
/**
*Connects the tank to the engine
*Warns if already connected or there is no such tank
*/
void Engine::connect_fuel_tank_to_engine(int tank_id) {
	int control = 0;
	for (int i = 0; i < tankcount; i++) {
		if (tank.getId(i) == tank_id)
		{
			if (tankArray[i].getIsconnect(i) == 1) {
				output.Endl();
				output.printFile("The tank is already connected !");
				break;
			}
			else {
				tank.Connect(i, 1);
				count_of_tank++;
				control++;
				break;
			}
		}
	}
	if (control == 0) {
		output.Endl();
		strr = to_string(tank_id);
		output.printFile("There is no the tank that is id: " + strr);
	}
}
///disconnects the tank
void Engine::disconnect_fuel_tank_from_engine(int tank_id) {
	int control = 0;
	for (int i = 0; i < tankcount; i++) {
		if (tank.getId(i) == tank_id)
		{
			if (tankArray[i].getIsconnect(i) == 0) {
				output.Endl();
				output.printFile("The tank is already disconnected!");
				break;
			}
			else {
				tank.Connect(i, 0);
				count_of_tank--;
				control++;
				break;
			}
		}
	}
	if (control == 0) {
		output.Endl();
		strr = to_string(tank_id);
		output.printFile("There is no the tank that is id: " + strr);
	}
}
double Engine::internalTankcapacity;
double Engine::internalTankfuel;
/**
*follows and fills the amount of gasoline in the internal tank, recalls when it falls below 20
*/
void Engine::internalTank()
{
	int controlInteral = 0.0;
	controlInteral = internalTankfuel;
	for (int i = 0; i < tankcount; i++) {
		if (tankArray[i].getIsconnect(i) == 1 && tankArray[i].getbroke(i) == 0 &&valve[i].get_status()==1)
		{
			if (internalTankfuel == 0) //fills if the tank is empty at first
			{
				if (tankArray[i].getFuel_quantity(i) >= 20 && tankArray[i].getFuel_quantity(i) <= internalTankcapacity)
				{
					internalTankfuel = tankArray[i].getFuel_quantity(i);
					tankArray[i].settankFuel(i, tankArray[i].getFuel_quantity(i));
					break;
				}
				else if (tankArray[i].getFuel_quantity(i) >= internalTankcapacity)
				{
					internalTankfuel = internalTankcapacity;
					tankArray[i].settankFuel(i, internalTankcapacity);
					break;
				}
			}
			if (tankArray[i].getFuel_quantity(i) > 0 && tankArray[i].getFuel_quantity(i) + internalTankfuel <= internalTankcapacity)
			{
				internalTankfuel += tankArray[i].getFuel_quantity(i);
				tankArray[i].settankFuel(i, tankArray[i].getFuel_quantity(i));
				//If the total of tank and internal is less than capacity, all go online.
				break;
			}
			else if (tankArray[i].getFuel_quantity(i) > 0 && tankArray[i].getFuel_quantity(i) + internalTankfuel > internalTankcapacity)
			{
				double tempquantity = internalTankcapacity - internalTankfuel;//Determines the gasoline requirement of the internal tank
				internalTankfuel = internalTankcapacity;
				tankArray[i].settankFuel(i, tempquantity);
				break;
				//If the sum of the tank and internal is more than the capacity, it is reduced as necessary.
			}
			else if (tankArray[i].getFuel_quantity(i) == 0)
				continue; //Tank scanning continued if not enough gasoline
		}
	}
	if (controlInteral == internalTankfuel) {
		output.printFile("Not enough gasoline in tanks!");
		// engine is stopped because there is no gasoline
		stop_engine();
	}

}
///consumes gas, must be called every command
void Engine::Spend_Fuel() {
	if (status == 1) {
		if (internalTankfuel <= 20)
		{
			internalTank();
		}
		internalTankfuel = internalTankfuel - 5.5;
		consumed_fuel += 5.5;
	}
}
///burns gasoline for the given time
//per second
void Engine::wait(int seconds) {
	for (int i = 0; i < seconds; i++) {
		if (status == 1) {
			Spend_Fuel();
		}
	}
}
/**
*Transmits the gasoline in the internal tank to the tank containing the least gasoline before the engine stops
*When called in command, it transfers the amount of gasoline to the tank with minimum gasoline
*/
void Engine::give_back_fuel(double fuel_quantity) {

	int k = 0;
	if (fuel_quantity != 0.0)
	{
		for (int i = 1; i < tankcount; i++)
		{
			double min = tankArray[i - 1].getFuel_quantity(i - 1);
			if (tankArray[i].getFuel_quantity(i) < min)
			{
				min = tankArray[i].getFuel_quantity(i);
				int k = i;
			}
		}
		int tankid = tankArray[k].getId(k);
		tank.fill_tank(tankid, fuel_quantity);
	}

}
//for singleton
Engine* Engine::GetInstance(Subject *sub) {

	if (!instance)
		instance = new Engine(sub);
	return instance;
}

void Engine::update()
{
	output.Endl();
	output.printFile("Engine: Simulation stopped.");
}
