#pragma once
#include "Engine.h"
#include "Tank.h"
#include <string>
/*! \brief tankArray is an array.
*
*	This stores created tanks in simulation.
*
*/
extern Tank* tankArray;
/*! \brief Create function.
*
*	This creates tankArray.
*
*/
void createArray();
class Simulation
{
	/*! \class Simulation Simulation.h
	 *  \brief This is a simulation class.
	 *
	 * Simulation class takes user's input commands, understand them and distribute them to other classes or functions.
	 * It alsa position other classes to their operations.
	 * This class has Engine object and Tank objects.
	 *
	 */
private:
	/*! \brief This is a array.
	*
	*	commandArr is a dynamic array which stores inputs line by line.
	*
	*/
	std::string* commandArr;
	/*! \brief This is a size variable.
	*
	*	size indicates commandArr's first set size.
	*
	*/
	size_t size = 20;
public:
	Simulation();
	~Simulation();
	/*! \brief This is a read file function.
	*
	*	read_file function reads file from entered argument.
	*	It stores inputs line by line in array.
	*
	*/
	void read_file(char* fileName);
	/*! \brief This is a distribute function.
	*
	*	distribute_commands checks command is valid or not from array. If valid, calls connected function from class.
	*
	*/
	void distribute_commands();
	/*! \brief This is a stop function.
	*
	*	stop_simulation is called from distribute_commands function.
	*	distribute_commands function calles this function when "stop_simulation;" command comes from user's command (input) file.
	*	It has only exit function. This stops program.
	*
	*/
	void stop_simulation();
};
