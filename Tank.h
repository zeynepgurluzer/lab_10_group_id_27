#include"Valve.h"
#include"Obs.h"
#pragma once
/*! \brief This is an extern integer variable.
*
*	"tankcount" keeps count of tank.
*
*/
extern int tankcount;

/*! \brief This is an extern double variable.
*
*	"consumed_fuel" keeps total of consumed fuel.
*
*/
extern double consumed_fuel;

class Tank : public Observer
{
	/*! \class Tank Tank.h
	 *  \brief This is a tank class.
	 *
	 * Tank class adds new tank to array, contains information of tanks and if the program needs, it can be print to screen.
	 * It calculates total consumed fuel from Engine.
	 * Tank class can remove objects if the program needs.
	 * Tank class can controls tank's current situation, valve and so on.
	 * Otherwise, this class contains get functions. Get functions is for Engine class.
	 *
	 */
private:
	/*! \brief This is a double variable.
	*
	*	"capacity" keeps capacity of tank.
	*
	*/
	double capacity;

	/*! \brief This is a double variable.
	*
	*	"fuel_quantity" keeps fuel quantity of tank.
	*
	*/
	double fuel_quantity;

	/*! \brief This is a double variable.
	*
	*	"total_fuel" keeps total fuel quantity of tanks.
	*
	*/
	double total_fuel = 0;

	/*! \brief This is a boolean variable.
	*
	*	"broken" controls tank's current situation.
	*   If tank is broken, then "broken" is true.
	*
	*/
	bool broken;

	/*! \brief This is an integer variable.
	*
	*	"id" means tank id.
	*
	*/
	int id;

	/*! \brief This is a boolean variable.
	*
	*	"isconnect" controls if the tank is connected to engine.
	*   If tank is connected to engine, then "isconnect" is true.
	*
	*/
	bool isconnect;

	/*! \brief This is a Tank pointer object.
	*
	*	"tank" object keeps information of each tank.
	*
	*/
	Tank* tank;
public:

	Tank();
	Tank(Subject *sub);
	~Tank();

	/*! \brief This is a function adds new tanks.
	*
	*	@param capacity The tank's capacity
	*   \return nothing
	*
	*/
	void add_fuel_tank(double capacity, Subject sub);

	/*! \brief This is a function controls if the code adds fuel or not.
	*
	*	@param tank_id, fuel_quantity
	*   \return nothing
	*
	*/
	void fill_tank(int tank_id, double fuel_quantity);

	/*! \brief This is a function lists tanks.
	*
	*	@param nothing
	*   \return nothing
	*
	*/
	void list_fuel_tanks();

	/*! \brief This is a function prints tanks.
	*
	*	@param nothing
	*   \return nothing
	*
	*/
	void print_fuel_tank_count();

	/*! \brief This is a function removes the tank.
	*
	*	@param tank_id
	*   \return nothing
	*
	*/
	void remove_fuel_tank(int tank_id);

	/*! \brief This is a function breaks the tank.
	*
	*	@param tank_id
	*   \return nothing
	*
	*/
	void break_fuel_tank(int tank_id);

	/*! \brief This is a function repairs the tank.
	*
	*	@param tank_id
	*   \return nothing
	*
	*/
	void repair_fuel_tank(int tank_id);

	/*! \brief This is a function prints connected tanks.
	*
	*	@param nothing
	*   \return nothing
	*
	*/
	void list_connected_tanks();

	/*! \brief This is a function prints total of fuel quantity.
	*
	*	@param nothing
	*   \return nothing
	*
	*/
	void print_total_fuel_quantity();

	/*! \brief This is a function prints total of consumed fuel quantity.
	*
	*	@param nothing
	*   \return nothing
	*
	*/
	void print_total_consumed_fuel_quantity();

	/*! \brief This is a function prints informations of tanks.
	*
	*	@param tank_id
	*   \return nothing
	*
	*/
	void print_tank_info(int tank_id);

	/*! \brief This is a function gets id.
	*
	*	@param index
	*   \return tankArray[index].id
	*
	*/
	int getId(int index);

	/*! \brief This is a function controls the tank is connected or not.
	*
	*	@param index, control
	*   \return nothing
	*
	*/
	void Connect(int index, int control);

	/*! \brief This is a function gets tank's current situation.
	*
	*	@param index
	*   \return tankArray[index].broken
	*
	*/
	bool getbroke(int index);

	/*! \brief This is a function gets tank's fuel quantity.
	*
	*	@param index
	*   \return tankArray[index].fuel_quantity
	*
	*/
	double getFuel_quantity(int index);

	/*! \brief This is a function gets situation of connect.
	*
	*	@param index
	*   \return tankArray[index].isconnect
	*
	*/
	int getIsconnect(int index);

	/*! \brief This is a function sets quantity.
	*
	*	@param index, quantity
	*   \return nothing
	*
	*/
	void settankFuel(int index, double quantity);

	/*! \brief This is a function about Observer.
	*    
	*   @param nothing
	*   \return nothing
	* 
	*/
	void update();
};
