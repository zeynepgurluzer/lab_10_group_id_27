#include "Tank.h"
#include "Simulation.h"
#include<iostream>
#include "Output.h"
#define SIZE 20
using namespace std;
Valve* valve = new Valve[SIZE];
int tankcount = 0;
double consumed_fuel = 0.0;
string stri;
Tank::Tank(){}
Tank::Tank(Subject* sub) : Observer(sub) {

	capacity = 0;
	fuel_quantity = 0;
	broken = false;
	id = 1;
	isconnect = 0;

}
Tank::~Tank() { }//delete valve; }}

void Tank::add_fuel_tank(double capacity, Subject sub) {
	tank = new Tank(&sub);
	tank->capacity = capacity;
	tank->id = id;
	tankArray[tankcount] = *tank;
	valve->add_valve(tankcount,&sub);
	output.Endl();
	stri = to_string(id);
	output.printFile("Tank "+stri+" is added");
	id++;
	tankcount++;
}

void Tank::fill_tank(int tank_id, double fuel_quantity) {
	for (int i = 0; i < tankcount; i++) {
		if (tankArray[i].id == tank_id) {
			double empty = tankArray[i].capacity - tankArray[i].fuel_quantity;
			if (empty < fuel_quantity)
			{
				output.Endl();
				output.printFile("There is no enough capacity to fill!");
				break;
			}
			tankArray[i].fuel_quantity += fuel_quantity;
			total_fuel += fuel_quantity;
			break;
		}
	}
}
void Tank::list_fuel_tanks() {
	output.Endl();
	output.printFile("---List of Fuel Tanks---");
	for (int i = 0; i < tankcount; i++) {
		stri=to_string(tankArray[i].id);
		output.printFile("Tank " + stri);
	}
}

void Tank::print_fuel_tank_count() {
	output.Endl();
	stri = to_string(tankcount);
	output.printFile("Count of Fuel Tank: " + stri);
}

void Tank::remove_fuel_tank(int tank_id) {
	for (int i = 0; i < tankcount; i++) {
		if (tankArray[i].id == tank_id) {
			*tank = tankArray[i];
			delete tank;
			tankcount--;

			string stri = to_string(tankArray[i].id);
			output.Endl();
			output.printFile("Tank " + stri + " is removed.");
			for (int j = i; j < tankcount; j++) {
				tankArray[j] = tankArray[j + 1];
			}
		}
	}
}

void Tank::break_fuel_tank(int tank_id) {
	if (broken == true) {
		stri = to_string(tank_id);
		output.printFile("Tank " + stri + " is already broken!");
	}

	else {
		for (int i = 0; i < tankcount; i++) {
			if (tankArray[i].id == tank_id) {
				tankArray[i].broken = true;
				break;
			}
		}
	}

}

void Tank::repair_fuel_tank(int tank_id) {
	if (broken == false) {
		output.Endl();
		stri = to_string(tank_id);
		output.printFile("Tank " + stri + " is not broken!");
	}

	else {
		for (int i = 0; i < tankcount; i++) {
			if (tankArray[i].id == tank_id) {
				tankArray[i].broken = false;
				break;
			}
		}
	}
}

void Tank::list_connected_tanks() {
	bool flag = false;
	stri;
	output.Endl();
	output.printFile("---Connected Tanks---");
	for (int i = 0; i < tankcount; i++) {
		if (tankArray[i].isconnect == true) {
			stri = to_string(tankArray[i].id);
			output.printFile("Tank " + stri);
			flag = true;
		}
	}

	if (flag == false) {
		output.Endl();
		output.printFile("Not found connected tank!");
	}
}

void Tank::print_total_fuel_quantity() {
	output.Endl();
	stri = to_string(total_fuel);
	output.printFile("Total Fuel Quantity: " + stri);
}

void Tank::print_total_consumed_fuel_quantity() {
	output.Endl();
	stri = to_string(consumed_fuel);
	output.printFile("Total Consumed Fuel: " + stri);
}

void Tank::print_tank_info(int tank_id) {
	if (tankArray == 0) {
		output.Endl();
		output.printFile("There is no tank to information!");
	}

	else {
		for (int i = 0; i < tankcount; i++) {
			if (tankArray[i].id == tank_id) {
				output.Endl();
				stri = to_string(tank_id);
				output.printFile("---Tank " + stri + " Indormation---");
				stri = to_string(tankArray[i].capacity);
				output.printFile("Capacity: " + stri);
				stri = to_string(tankArray[i].fuel_quantity);
				output.printFile("Fuel Quantity: " + stri);
				if (tankArray[i].broken == true)
					output.printFile("Current Situation: Broken");
				else
					output.printFile("Current Situation: Unbroken");
				if (valve[i].get_status() == true)
					output.printFile("Valve: Opened");
				else
					output.printFile("Valve: Not Opened");
				break;
			}
		}
	}

}
int Tank::getId(int index) {
	return tankArray[index].id;
}

void Tank::Connect(int index, int control) {
	tankArray[index].isconnect = control;
}

bool Tank::getbroke(int index) {
	return tankArray[index].broken;
}


double Tank::getFuel_quantity(int index) {
	return tankArray[index].fuel_quantity;
}

int Tank::getIsconnect(int index) {
	return tankArray[index].isconnect;
}

void Tank::settankFuel(int index, double quantity)
{
	tankArray[index].fuel_quantity -= quantity;
}

void Tank::update()
{
	static int i = 1;
	stri = to_string(i);
	output.printFile("Tank" + stri + ": simulation stopped.");
	i++;
}
