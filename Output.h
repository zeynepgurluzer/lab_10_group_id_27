#pragma once
#include<fstream>
#include<iostream>
using namespace std;
class Output {
private:
    /*!< file defined for file opening and writing*/
    ofstream file;
public:
 /*!
 \param openFile a void function
 \used to open file
 \return nothing
*/
    void openFile();
 /*!
 \param printFile a void function
 \used for printing to file
 \return nothing
*/
    void printFile(string);
 /// used to skip a line
    void Endl();
 /*!
 \param closeFile a void function
 \used to close file
 \return nothing
*/
    void closeFile();
};
/*!< extern output object defined for all classes to use same object */
extern Output output;