#include "Subject.h"
class Observer;
void Subject::notify() {

    for (int i = 0; i < views.size(); i++)
        views[i]->update();
}