/**
* SILA EŞME_152120181004
* MERVE KATI_152120171025
* İPEK BİRİNCİ_152120191020
* SANEM YILDIZ KAVUKOĞLU_152120181043
* ZEYNEP GÜRLÜZER_152120191050
*/
#include<iostream>
#include"Simulation.h"
#include "Engine.h"
#include "Output.h"
using namespace std;
Output output;
extern char* fileName;
int main(int argc, char* argv[])
{
	Simulation S;

	S.read_file((argv[1]));
	fileName = argv[2];
	output.openFile();
	S.distribute_commands();
	output.closeFile();
	system("pause");
}
