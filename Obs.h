#pragma once
#include "Subject.h"
class Subject;
class Observer
{
    Subject* model; /*< This member provides attach and detach Observer objects. */
public:
    Observer();
    /** In this constructor, member variable is setted.
    */
    Observer(Subject* mod);
    /** Pure virtual function.
    */
    virtual void update() = 0;
};
