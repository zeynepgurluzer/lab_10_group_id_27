#include<iostream>
#include "Valve.h"
#include "Tank.h"
#include "Simulation.h"
#include "Output.h"
using namespace std;
string ostr;
Valve::Valve()
{
	valve_status = false;
}
Valve::Valve(Subject *sub): Observer(sub) { valve_status = false; }
void Valve::set_status(bool status) { valve_status = status; }
void Valve::open_valve(int tank_id) {
	for (int i = 0; i < tankcount; i++) {
		if (tankArray[i].getId(i) == tank_id) {
			if (valve[i].valve_status == true) {
				output.Endl();
				ostr = to_string(tank_id);
				output.printFile("Tank " + ostr + "'s valve is already opened!");
				break;
			}
			else {
				valve[i].valve_status = true;
				break;
			}
		}
	}
}
void Valve::close_valve(int tank_id) {

	for (int i = 0; i < tankcount; i++) {
		if (tankArray[i].getId(i) == tank_id) {
			if (valve[i].valve_status == false) {
				output.Endl();
				ostr = to_string(tank_id);
				output.printFile("Tank " + ostr + "'s valve is already closed!");
				break;
			}
			else {
				valve[i].valve_status = false;
				break;
			}
		}
	}

}
void Valve::add_valve(int index, Subject *sub) {
	Valve* valve1;
	valve1 = new Valve(sub);
	valve[index] = *valve1;
}
bool Valve::get_status() { return valve_status; }
void Valve::update()
{
	static int i = 1;
	ostr = to_string(i);
	output.printFile("Valve " + ostr + ": simulation stopped.");
	i++;
}

