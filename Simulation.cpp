#include "Simulation.h"
#include "Engine.h"
#include "Tank.h"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "Output.h"
Tank* tankArray;
size_t SIZE = 20;
string sstr;

void createArray()
{
	tankArray = new Tank[SIZE];
}

Engine* Engine::instance = NULL;

using namespace std;

Simulation::Simulation() {
	commandArr = new string[size];
}
Simulation::~Simulation() { delete[] commandArr; delete[] tankArray; }
void Simulation::read_file(char* fileName) {
	ifstream file;
	string line;

	file.open(fileName);
	sstr = fileName;
	if (file.is_open()) {
		output.printFile(sstr + " file is opened.");
		int i = 0;
		while (getline(file, line)) {
			if (i == size - 1)
			{
				this->size = this->size * 2;
				string* tempArr = new string[this->size];
				for (int j = 0; j < size / 2; j++) tempArr[j] = this->commandArr[j];
				delete[] this->commandArr;
				this->commandArr = tempArr;
			}
			commandArr[i] = line;
			i++;
		}
		file.close();
	}
	else
		output.printFile(sstr + " File cannot open!");
	
}
void Simulation::distribute_commands()
{
	Subject sub;
	Tank tank(&sub);
	Valve valve(&sub);
	sub.detach(&valve); // detach this because we don't want to temp valve to the list
	sub.detach(&tank);  // detach this because we don't want to temp valve to the list
	createArray();
	Engine* engine = Engine::GetInstance(&sub);

	string tempCommand;
	double tempParameter = NULL, tempParameter2 = NULL;
	for (int i = 0; i < size; i++)
	{
		stringstream command(commandArr[i]);

		command >> tempCommand >> tempParameter >> tempParameter2;

		/***********COMMAND DISTRUBUTE AREA***********/

		///This If block checks is command line equals nonparameted commands
		if (tempParameter != NULL)
		{
			if (tempCommand == "print_tank_info")
			{
				int parameter = tempParameter;
				tank.print_tank_info(parameter);
				engine->Spend_Fuel();
			}
			else if (tempCommand == "open_valve")
			{
				int parameter = tempParameter;
				valve.open_valve(parameter);
				engine->Spend_Fuel();
			}
			else if (tempCommand == "close_valve")
			{
				int parameter = tempParameter;
				valve.close_valve(parameter);
				engine->Spend_Fuel();
			}
			else if (tempCommand == "wait")
			{
				int parameter = tempParameter;
				engine->wait(parameter);
				engine->Spend_Fuel();
			}
			else if (tempCommand == "give_back_fuel")
			{
				engine->give_back_fuel(tempParameter);
				engine->Spend_Fuel();
			}
			else if (tempCommand == "add_fuel_tank")
			{
				/***IF ARRAY OF TANKS IS FULL, EXPAND IT***/
				if (tankcount == SIZE + 1)
				{
					SIZE = SIZE * 2;
					Tank* tempArr = new Tank[SIZE];
					for (int j = 0; j < SIZE / 2; j++) tempArr[j] = tankArray[j];
					delete[] tankArray;
					tankArray = tempArr;
				}
				/*****************************************/
				sub.attach(&tank);
				sub.attach(&valve);
				tank.add_fuel_tank(tempParameter, sub);
				engine->Spend_Fuel();
			}
			else if (tempCommand == "fill_tank")
			{
				int parameter = tempParameter;
				tank.fill_tank(parameter, tempParameter2);
				engine->Spend_Fuel();
			}

			else if (tempCommand == "remove_fuel_tank")
			{
				int parameter = tempParameter;
				sub.detach(&valve);
				sub.detach(&tank);
				tank.remove_fuel_tank(parameter);
				engine->Spend_Fuel();
			}
			else if (tempCommand == "connect_fuel_tank_to_engine")
			{
				int parameter = tempParameter;
				engine->connect_fuel_tank_to_engine(parameter);
				engine->Spend_Fuel();
			}

			else if (tempCommand == "disconnect_fuel_tank_from_engine")
			{
				int parameter = tempParameter;
				engine->disconnect_fuel_tank_from_engine(parameter);
				engine->Spend_Fuel();
			}
			else if (tempCommand == "break_fuel_tank")
			{
				int parameter = tempParameter;
				tank.break_fuel_tank(parameter);
				engine->Spend_Fuel();
			}
			else if (tempCommand == "repair_fuel_tank")
			{
				int parameter = tempParameter;
				tank.repair_fuel_tank(parameter);
				engine->Spend_Fuel();
			}
			else {output.printFile("Invalid command!"); }
		} //endif

		/******************************************************************/

		///This Else block checks is command line equals parameted commands
		else {
			if (tempCommand == "start_engine;")
			{
				engine->start_engine();
				engine->Spend_Fuel();
			}
			else if (tempCommand == "print_fuel_tank_count;")
			{
				tank.print_fuel_tank_count();
				engine->Spend_Fuel();
			}
			else if (tempCommand == "print_total_fuel_quantity;")
			{
				tank.print_total_fuel_quantity();
				engine->Spend_Fuel();
			}
			else if (tempCommand == "print_total_consumed_fuel_quantity;")
			{
				tank.print_total_consumed_fuel_quantity();
				engine->Spend_Fuel();
			}
			else if (tempCommand == "list_fuel_tanks;")
			{
				tank.list_fuel_tanks();
				engine->Spend_Fuel();
			}
			else if (tempCommand == "list_connected_tanks;")
			{
				tank.list_connected_tanks();
				engine->Spend_Fuel();
			}
			else if (tempCommand == "stop_engine;")
			{
				engine->stop_engine();
				engine->Spend_Fuel();
			}
			else if (tempCommand == "stop_simulation;")
			{

				sub.notify();
				stop_simulation();
			}
			else { output.printFile(commandArr[i] + "- Invalid command!"); }
		} //endif

		/**********COMMAND DISTRUBUTE AREA ENDS**********/


		/*Zeroing temporary variables*/
		tempCommand = "";
		tempParameter = NULL;
		tempParameter2 = NULL;
		/*****************************/
	}
}
void Simulation::stop_simulation() { exit(0); }
