#include "Output.h"
#include <fstream>
char* fileName;
void Output::openFile() {
	if (file.is_open()) { 
		cout << fileName << " file is already opened!" << endl;
	}
	else { //file is opened,firstly.
		file.open(fileName);
	}

}
///prints the incoming string
void Output::printFile(string s) {
	file << s << endl;
}
void Output::Endl() {
	file << endl;
}
void Output::closeFile() {
	file.close();
}
