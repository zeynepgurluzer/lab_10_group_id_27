#pragma once
#include <vector>
#include "Obs.h"
class Observer;
/** This class includes Observe operations.
*/
class Subject
{
    std::vector < Observer* > views; /*< This array includes active observers.*/
public:
    /** This function adds observers to the @var views.
    * @param *obs
    * : Given observer.
    */
    void attach(Observer* obs) {
        views.push_back(obs);
    }
    /**This function deletes observers to the @var views.
    * @param* obs
    * : Given observer.
    */
    void detach(Observer* obs) {
        size_t i = 0;
        for (; i < views.size(); i++)
        {
            if (views[i] == obs)
            {
                views.erase(views.begin() + i);
                break;
            }
        }
    }
    /**This function calls overwritten update() functions from related Observer.
    */
    void notify();
};
