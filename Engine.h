#pragma once
#include<string>
#include "Tank.h"
#include "Obs.h"
class Engine:public Observer
{
private:
	const double fuel_per_second;
	/// if status and isconnect are both zero, engine doesn't work
	/// status represents start and stop
	bool status;
	/// count_of_tank counts number of tanks
	static int count_of_tank;
	static double internalTankcapacity; //for internal tank capacity
	static double internalTankfuel;
	static Engine* instance;
	Tank tank;
	Engine();
	Engine(Subject* sub);

public:
	~Engine();
	void start_engine();
	void statusControl();
	void stop_engine();
	void connect_fuel_tank_to_engine(int tank_id); //int or Tank
	void disconnect_fuel_tank_from_engine(int tank_id); //int or Tank
	void internalTank(); //to follow internal tank fuel quantity 
	void Spend_Fuel();
	void wait(int);
	void give_back_fuel(double);
	static Engine* GetInstance(Subject* sub);
	void update();
};
#pragma once
